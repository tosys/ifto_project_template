from django.conf.urls import include, url, 
from django.contrib import admin


# Para django 1.9 ou 1.10 or 1.11
urlpatterns = (
    # Examples:
    # url(r'^$', '{{ project_name }}.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
